# IPK Calculator Client Documentation

The IPK Calculator Client is a command-line interface program that allows you to communicate with computation server using IPKCP protocol.
This client can be used to send commands to the server, receive responses, and display them on the standard output.

## Usage

To use the IPK Calculator Client, run the `ipkcpc` executable with the following options:

``` sh
./ipkcpc [-m mode] [-h host] [-p port]
```
Where:

- `host` is the IPv4 address of the server
- `port` is the server port
- `mode` is the communication mode to be used (`tcp` or `udp`)

Once the client is running, it expects command on standard input, one per line.
The client will display the responses from the server on standard output.

If the input is being read from terminal, prompt ("> ") is displayed to indicate, that the client is ready for next command.

## Testing

Because the client isn't much more than a front end to a socket testing is simple.
At least for TCP.
TCP testing is done using makefile target, which creates named pipe, create netcat process listening on port, saving everything it receives to `server.out` and sending it back.
Client reads data from `test_data` and saves stdout to `client.out`.
After that simple diff is used to check that the data did not change.
Testing data is in `test_data` and  is generated every time from base64 encoded random data.

For UDP I've used custom made packets in `udp_tests` directory, which I've sent to client using netcat.
To automate, at least a little bit, this I've created `test_udp.sh` script, which sends all testing packets from `udp_tests` directory and prints hexdump of what server sent and received.
I've then manually checked the output.

## Implementation

Whole program is split into three modules - main, net and util.

### main.c

- `main` - program entry, checks that arguments were processed correctly, socket was created and returns result of client-server communication
- `get_opts` - parses command line arguments using `getopt` functions from standard library
- `loop` - main loop, responsible for reading from stdin and sending said line to `net` module, return code of `loop` should be program's exit code

Module `main` also uses global variable `run` which is set to false by `sigint_handler` - callback called when receiving SIGINT

### net.c

`net` module defines wrapper for socket which hold socket itself, info if it's tcp or udp and for udp server address

- `connect_socket` - establishes tcp or udp connection to server, returns 0 on suecess and socket in function argument
- `socket_read` - waits for data from server, return length on suecess, -1 on failure, calls `check_udp` for udp packets
- `check_udp` - checks if udp response has correct header
- `socket_write` - sends data to server, return 0 on suecess, -1 on failure

### util.c

util contains miscellaneous functions (in this case only `err` - function that prints to stderr)

## Authors

- Matěj Vrba <xvrbam03@stud.fit.vutbr.cz>

## License

This project is licensed under the [GPLv3 license](https://www.gnu.org/licenses/)
