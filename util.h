#ifndef UTIL_H
#define UTIL_H
#include <getopt.h> // for optarg
#include <errno.h> // for errno
#include <getopt.h> // for getopt_long, option, required_argument
#include <signal.h> // for signal, SIGINT
#include <stdarg.h> // for va_end, va_list, va_start
#include <stdbool.h> // for false
#include <stdio.h> // for printf, perror, NULL, fileno, fprintf
#include <stdlib.h> // for free, malloc, strtol, EXIT_FAILURE
#include <string.h> // for strcmp, strlen, strcpy
#include <unistd.h> // for close, isatty
#include "util.h"

// prints error, expects printf-like string as first argument
// optionally other arguments just like printf :)
void err(char *msg, ...);

#endif /* UTIL_H */
