/*
    Copyright (C) 2023  Matěj Vrba

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#define _POSIX_C_SOURCE 200809L
#include <getopt.h> // for optarg
#include <errno.h> // for errno
#include <getopt.h> // for getopt_long, option, required_argument
#include <signal.h> // for signal, SIGINT
#include <stdarg.h> // for va_end, va_list, va_start
#include <stdbool.h> // for false
#include <stdio.h> // for printf, perror, NULL, fileno, fprintf
#include <stdlib.h> // for free, malloc, strtol, EXIT_FAILURE
#include <string.h> // for strcmp, strlen, strcpy
#include <unistd.h> // for close, isatty
#include "net.h" // for socket_write, socket_t, connect_socket
#include "util.h" //for err
// true(1) until SIGINT (C-c) is received
// receiving SIGINT changes run to false(0)
int run = 1;


void sigint_handler(int signal)
{
	run = 0;
}

int loop(socket_t sock)
{
	signal(SIGINT, sigint_handler);
	char *line = NULL, read_buff[256];
	size_t line_len = 0;
	int response_len;

	while (run) {
		// if stdin is from terminal, not file, print prompt
		if (isatty(fileno(stdin)))
			printf("> ");
		// attempt to read a line
		errno = 0;
		if ((getline(&line, &line_len, stdin) == -1)) {
			// reading failed
			if (run) { // run == true -> getline failed, print error and exit
				// if errno is non-zero an error occured
				// if errno is zero, getline read whole file
				if (errno == 0) {
					break;
				} else {
					perror("Failed to read line");
					goto return_fail;
				}
			} else // run == false -> program received SIGING (C-c)
				break;
		}else{
			// because IPKCP defines length as one byte, maximum length is 254 characters + null byte
			if(strlen(line) >= 255){
				err("Input line is too long, input cannot be longer than 254 characters");
				goto return_fail;
			}
		}
		if (strcmp(line, "BYE\n") == 0) break;
		if (socket_write(&sock, line, strlen(line)) == -1) {
			// check if writing failed because process received SIGINT or otherreason
			if (run)
				goto return_fail; // error is aleready printed, exit
			else
				break;
		}
		if ((response_len = socket_read(&sock, read_buff,
						sizeof(read_buff))) == -1) {
			// same as while reading
			if (run)
				goto return_fail;
			else
				break;
		}
		if (run) {
			printf("Response: %.*s", response_len, read_buff);
			if (sock.tcp == false)
				printf("\n");
		}
	}// while(run)
	if(sock.tcp)
		socket_write(&sock, "BYE", strlen("BYE"));

	close(sock.socketfd);
	free(line);
	return 0;
return_fail:
	close(sock.socketfd);
	free(line);
	return EXIT_FAILURE;
}

// checks command line arguments, return 0 if program should continue, -1 if it
// should exit (e.g. when --help is passed)
int get_opts(int argc, char **argv, char **host, int *port, int *tcp)
{
	int c, option_index;
	// set defaults
	*port = 2023;
	*tcp = 1;
	*host = NULL;

	// long options
	struct option long_options[] = { { "host", required_argument, 0, 0 },
					 { "port", required_argument, 0, 0 },
					 { "mode", required_argument, 0, 0 },
					 { "help", no_argument, 0, 0 },
					 { 0, 0, 0, 0 } };
	//loop until we read all options
	while (1) {
		option_index = 0;
		c = getopt_long(argc, argv, "h:p:m:H", long_options,
				&option_index);
		// check if we processed all options
		if (c == -1)
			break;
		const char *option_name;
		// check what current option
		switch (c) {
			// if long version was used, c is set to 0
			// if short version was used c is set to ascii value of that option
		case 0:
			option_name = long_options[option_index].name;
			// if long version was used, c is 0 and strcmp is used to determine what option it is.
			// if short version was used, switch jumps to coresponding case skipping the strcmp

			// check what mode is used
			if (strcmp(option_name, "mode") == 0) {
			case 'm':
				if (strcmp(optarg, "tcp") == 0) {
					*tcp = 1;
				} else if (strcmp(optarg, "udp") == 0) {
					*tcp = 0;
				} else {
					err("Unrecognised mode: %s", optarg);
					return -1;
				}
				break;

				// host option was passed, ip/hostname is copied into `host` buffer
			} else if (strcmp(option_name, "host") == 0) {
			case 'h':
				*host = malloc(strlen(optarg) + 1);
				strcpy(*host, optarg);
				break;

				// attempt to parse argument of port as base 10 number
			} else if (strcmp(option_name, "port") == 0) {
			case 'p':
				//strtol sets errno on failure
				errno = 0;
				*port = strtol(optarg, NULL, 10);
				if (errno) {
					perror("Failed to parse port");
					return -1;
				}
				break;
				//print help info
			} else if (strcmp(option_name, "help") == 0) {
			case 'H':
				err("usage: %s [-m mode] [-h host] [-p port] [-H]", argv[0]);
				err("");
				err("\t-h --host\taddress of ipkpd server,         default: 127.0.0.1");
				err("\t-p --port\tport of ipkpd server,            default: 2023");
				err("\t-m --mode\tcommunication mode (tcp or udp), default: tcp");
				err("\t-H --help\tprint this message");
				return -1;
				break;
			}
		default:
			return -1;// invalid option
		}// switch
	}// while(1)
	//check if host was set
	if (*host == NULL) {
		*host = malloc(strlen("127.0.0.1") + 1);
		strcpy(*host, "127.0.0.1");
	}
	return 0;
}

int main(int argc, char **argv)
{
	char *host;
	int port, tcp;
	if(get_opts(argc, argv, &host, &port, &tcp) == -1)
		return EXIT_FAILURE;
	socket_t sock;
	if (connect_socket(&sock, host, port, tcp) != 0) {
		free(host);
		return EXIT_FAILURE;
	}
	free(host);
	return loop(sock);
}
