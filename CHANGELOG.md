# What's implemented
- Both tcp and udp mode. UDP tested on localhost, TCP tested on localhost and through SSH tunel
- all arguments should work in any order, all have long and short version and default value
- Incomming UDP packets are checked for correct header
- if input is a tty, "> " prompt is printed to indicate that the client is waiting for input from user

# What's missing/wrong
- When receiving BYE from server, client doesn't exit nor re-established connection
- using gotos - some consider it bad practice
- no doxygen comments - not planing on making them for project this small
