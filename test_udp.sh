#!/usr/bin/env bash

for i in ./udp_tests/*
do
		echo "======================="
		echo "test: $i"
		echo "======================="
		echo "server:   OP ST LN DATA" #data sent from server to client
		hexdump "$i" -C
		echo 'output from client (stderr is yellow): '
		nc -lu 127.0.0.1 2023 > client_packet < "$i" &
		# https://superuser.com/a/542075 anwser by slayedbylucifer on 2013-01-26
		echo "client query" | ./ipkcpc -h "127.0.0.1" -p 2023 -m udp 2> >(while read line; do echo -e "\e[33m$line\e[0m" >&2; done)
		echo "ipkcpc exit code: $?"
		echo "packet received by server: "
		hexdump client_packet -C
		killall nc
		rm client_packet
done
