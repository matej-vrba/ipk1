#include "util.h"
#include "util.h"
#include <stdarg.h> // for va_end, va_list, va_start
#include <stdio.h> // for fprintf, vfprintf, stderr

void err(char *msg, ...)
{
	va_list va_args;
	va_start(va_args, msg);
	vfprintf(stderr, msg, va_args);
	fprintf(stderr, "\n");
	va_end(va_args);
}
