##
# [TITLE]
#
# Authors:
# Mates Vrba <xvrbam03@stud.fit.vutbr.cz>
#
# IPK Calculator client
#
#
# Makefile usage:
# `make` or `make -j$(nproc) [--output-sync]` to build in debug
# `make -e DEBUG=false -j $(nproc)` to build release


TARGET = ipkcpc
# name of archive with source code generated by `make pack` (the name is
# ARCHIVE.tar)
ARCHIVE = xvrbam03
#list of files to add to archive (source code is aleready included)
AR_ADD = Makefile README.md *.h LICENSE CHANGELOG.md

SRC = $(wildcard *.c) # find c source files in current dir
OBJ = $(SRC:.c=.o) # every .c file will be converted to .o
# gcc generated .d file from every .c file containing it's dependencies
# (other header files)
DEP = $(SRC:.c=.d)

TEXI2PDF = texi2pdf # program used to convert texi files to pdf
SHELL = /bin/bash #just to be sure

CFLAGS = -std=c11
CFLAGS += -Wall -Wstrict-prototypes
#for flags to tell gcc to generate dependencies
CFLAGS += -MMD -MP
ifeq ($(DEBUG),true)
	LDFLAGS+= -fsanitize=address
	CFLAGS += -fsanitize=address
	CFLAGS += -g
	CFLAGS += -O0
else
	CFLAGS += -DNDEBUG
	CFLAGS += -O2
endif

LDFLAGS+= -lm

.PHONY: all clean run test test_tcp pack

all: $(TARGET)

-include $(DEP)

$(TARGET): $(OBJ)
	$(CC) $^ $(LDFLAGS) --output $@

run: $(TARGET)
	./$(TARGET)

clean:
	@echo Removing object files
	@- $(RM) $(OBJ)
	@echo Removing generated make rules
	@- $(RM) $(DEP)
	@echo Removing archive
	@- $(RM) $(ARCHIVE).zip
	@echo Removing files from testing
	@- $(RM) client.out server.out test_data test_fifo

# All tests must pass before archive is created
pack: test $(ARCHIVE).zip

$(ARCHIVE).zip: $(SRC) $(AR_ADD)
# check for TOODs and ignore this search
	@echo Checking fo TODOs
	[[ -z "$(grep -r TODO * | grep -v 'Makefile:	@grep -r TODO * | grep -v ' | grep -v 'Makefile:	@echo Checking fo TODOs')" ]]
	zip -r $@ $^

test: $(TARGET)
	make test_tcp -e 'CLIENT_FLAGS=-m tcp' -e 'NC_ARGS=-l 127.0.0.1 2023'
	make test_tcp -e 'CLIENT_FLAGS=-m tcp -p 1234' -e 'NC_ARGS=-l 127.0.0.1 1234'
	make test_tcp -e 'CLIENT_FLAGS=-m tcp -p 1234 -h 127.0.0.5' -e 'NC_ARGS=-l 127.0.0.5 1234'
	$(RM) test_fifo
	@echo "================"
	@echo "All tests passed"
	@echo "================"

test_tcp:
	@-rm test_data test_fifo 2> /dev/null
	@for i in {1..10}; do dd if=/dev/urandom of=/dev/stdout bs=1 count=128 2> /dev/null | base64 -w $$((1 + RANDOM % 128)) >> test_data; done
	@mkfifo test_fifo
	@cat test_fifo | nc $(NC_ARGS) | tee test_fifo | sed 's/^BYE$///' >server.out&
	@cat test_data | ./ipkcpc $(CLIENT_FLAGS) | sed 's/^Response: //' > client.out
	@diff test_data server.out
	@diff test_data client.out
