/*
    Copyright (C) 2023  Matěj Vrba

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "net.h"
#include "util.h" //for err
#include <arpa/inet.h> // for inet_addr, inet_ntoa
#include <netdb.h> // for gethostbyname, hostent
#include <stdlib.h> // for free, malloc
#include <string.h> // for memcpy, memmove
#include <sys/socket.h> // for connect, recv, recvfrom, sendto, socket, AF_...
#include <unistd.h> // for write
#include "util.h"

// writes data to udp or tcp socket
// expects initialized scoked struct defined in header, data and length of data to send.
// return 0 on suecess, non zero on failure, on failure prints to stderr
int socket_write(socket_t *sock, char *data, size_t data_len)
{
	int32_t written = 0, packet_len;
	int tmp = 0; // number of bytes written in each loop iteration

	while (written < data_len) { // send all the data
		if (sock->tcp) {
			if ((tmp = write(sock->socketfd, data, data_len)) ==
			    -1) {
				perror("Failed to write to socket");
				return 1;
			}
		} else {
			//2 - opcode and payload len
			packet_len = 2 + data_len;
			ipkcp_request_t *packet = malloc(packet_len);
			packet->opcode = 0;
			packet->len = data_len;
			memcpy(packet->data, data, data_len);

			if ((tmp = sendto(sock->socketfd, packet, packet_len, 0,
					  (struct sockaddr *)&sock->other_addr,
					  sizeof(sock->other_addr))) == -1) {
				perror("Failed to write to socket");
				return 1;
			}
			free(packet);
		}
		written += tmp;
	}

	return 0;
}

// checks header of udp packet received from server, on suecess prints nothing
// and returns 0, on failure prints error to stderr and returns non-zero
int check_udp(char *data, size_t len)
{
	char opcode = data[0];
	char status = data[1];
	unsigned char payload_len = data[2];
	//check if opcode is valid
	if (opcode != 1) {
		if (opcode == 0) {
			err("Received request instead of response");
		} else {
			err("Server returned invalid opcode");
		}
		return 1;
	}
	if (status == 1) {
		// prints all received payload data except header (+ 3)
		fprintf(stderr, "Server Error: %.*s\n", (int)len - 3, data + 3);
		return -1;
	}
	if (status != 0) {
		if(status == 1){
			err("Received error status code from server");
		}else{
			err("Received invalid status code from server");
		}
		return 2;
	}
	// length of received packet should be 3 bytes longer than length reported by server
	if (payload_len != len - 3) {
		err("Received invalid response length from server");
		return 3;
	}
	return 0;
}

// Receives packet from socket, writes received data to `data`
int socket_read(socket_t *sock, char *data, size_t data_len)
{
	int32_t num_read = 0;

	if (sock->tcp) {
		//read from tcp socet
		if ((num_read = recv(sock->socketfd, data, data_len - 1, 0)) == -1) {
			perror("Failed to read from socket");
			return -1;
		}
	} else {
		//read from udp socket
		if ((num_read = recvfrom(sock->socketfd, data, data_len - 1, 0,
					 &sock->other_addr, &sock->addr_len)) == -1) {
			perror("Failed to read from socket");
			return -1;
		}
		if (check_udp(data, num_read))
			return -1;
		// remove packet header
		memmove(data, data + 3, num_read - 3);
	}
	//null terminate the string, just to be sure
	data[num_read] = '\0';

	return num_read;
}

int connect_socket(socket_t *sock, char *server_hostname, uint16_t server_port,
		   bool tcp)
{
	if (sock == NULL)
		return 2;
	int sockfd;
	struct hostent *hostent;
	in_addr_t in_addr;
	struct sockaddr_in sockaddr_in;
	int sock_type;

	if (tcp)
		sock_type = SOCK_STREAM;
	else
		sock_type = SOCK_DGRAM;

	// Open da socket
	sockfd = socket(AF_INET, sock_type, 0);
	if (sockfd == -1) {
		perror("Failed to open socket");
		return -1;
	}

	// Resolve hostname
	hostent = gethostbyname(server_hostname);
	if (hostent == NULL) {
		err("Failed to resolve \"%s\"", server_hostname);
		return -1;
	}
	in_addr = inet_addr(inet_ntoa(*(struct in_addr *)*(hostent->h_addr_list)));
	if (in_addr == (in_addr_t)-1) {
		err("Failed to parse \"%s\" as an ipv4 address", *(hostent->h_addr_list));
		return -1;
	}

	sockaddr_in.sin_addr.s_addr = in_addr;
	sockaddr_in.sin_family = AF_INET;
	sockaddr_in.sin_port = htons(server_port);

	if (tcp) {
		// connect to the server
		if (connect(sockfd, (struct sockaddr *)&sockaddr_in,
			    sizeof(sockaddr_in)) == -1) {
			perror("Failed to connect to the server");
			return -1;
		}
	} else {
		// for udp save the server's address
		sock->other_addr = *(struct sockaddr *)&sockaddr_in;
		sock->addr_len = sizeof(sock->other_addr);
	}
	sock->socketfd = sockfd;
	sock->tcp = tcp;
	return 0;
}
