/*
    Copyright (C) 2023  Matěj Vrba

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef NET_H
#define NET_H
#include <netinet/in.h> // for sockaddr_in
#include <stdbool.h> // for bool
#include <stdint.h> // for uint8_t, uint16_t
#include <stdio.h> // for size_t

typedef struct ipkcp_request {
	uint8_t opcode;
	uint8_t len;
	uint8_t data[];
} ipkcp_request_t;

// struct containing socket filed and info if it's tcp or udp socket
typedef struct socket {
	int socketfd;
	bool tcp; // if true -> socket is tcp, udp otherwise
	// server address, used for udp communication.
	// For tcp sockets this is uninitialized
	struct sockaddr other_addr;
	socklen_t addr_len;
} socket_t;

// writes to tcp or udp socket
// expects socket struct, data and their length as arguments.
// returns 0 on suecess, -1 on failure
// on failure writes error to stderr
int socket_write(socket_t *sock, char *data, size_t data_len);
// check IPKCP udp response, returns 0 on suecess.
// on failure prints error to stderr and returns non-zero
// data - received byte array
// len - length of data as returned by recvfrom
int check_udp(char *data, size_t len);
// attempts to read from socket,
// expects socket, destination buffer and buffer size as arguments
// returns number of bytes read on suecess
// -1 on failure
int socket_read(socket_t *sock, char *data, size_t data_len);
// attempts to open a tcp or udp socket to `server_hostname`:`server_port`
// Sosocket is stored in `sock`
// returns 0 on suecess
int connect_socket(socket_t *sock, char *server_hostname, uint16_t server_port,
		   bool tcp);

#endif /* NET_H */
